# Snicker example repository

This repository is just a basic example for using Snicker in GitLab.

Our sample app is a basic JavaFX window (from https://docs.oracle.com/javafx/2/get_started/hello_world.htm), which was tested by Snicker with following steps:

1. On local workstation (eg. Ubuntu Linux):
  - __package app__ [with dependencies embeded]: 
      ```bash
      cd Snicker-HelloWorld-JavaFX
      javac HelloWorld.java
      ``` 
  - __install Snicker__ somewhere: `git clone https://gitlab.com/IRSN/Snicker`
  - [install Snicker dependencies]: 'docker', 'pkill', 'xtightvncviewer'
  - create Snicker test case: `sh Snicker/snicker.sh`
    -  [at first launch] it will create the __'snicker' container__
    - then it will launch its __VNC desktop window__:
      - with an 'xterm' window
      -  with a 'Snitch' window already recording your actions (minimized)
      - note that the host working dir is in '/WORKDIR' (where you should find your application to test)
    - __record your test__, following Snitch guidelines (https://gitlab.com/irsn/snitch-ci)
    - carefully select the __final screenshot__ for Snitch OCR
    - then __save your 'record.json'__ in '/WORKDIR' where it will persist over docker termination
    - __close XTerm__, which will also close VNC session and terminate docker
  - __replay your test case__: `sh Snicker/snicker.sh -f record.json -v` (you should see your test case playing, without interaction possible). It should return a '0' exit status if test passed
  - [if something goes wrong]: you can edit your test case (like adding delays between actions) using 'Snitch' (`pip3 install snitch-ci`) and loading 'record.json' file
2. On your GitLab repository, you have to push/append:
  - previous 'record.json' file
  - '.gitlab-ci.yml':
    ```yml
    image: ubuntu:18.04
    
    stages:
      - build
      - test
      - snitch
    
    build:
      stage: build
      script:
        - jdk1.8.0_251/bin/javac HelloWorld.java
      artifacts:
        paths:
        - ./*.class
        expire_in: 1 week
    
    test:
      stage: test
      dependencies:
        - build
      script:
        - echo "do some unit tests here"
     
    snitch:
      stage: snitch
      image: 
        name: irsn/snicker
        entrypoint: [""]
      dependencies:
        - build
      script:
        - rm -rf /WORKDIR && ln -s /builds/irsn/snicker-helloworld-javafx /WORKDIR
        - /bin/bash /snicker/run_snitch.sh -f record.json -d
      artifacts:
        paths:
        - ./log/
        - ./*.json
        - ./*.png
        expire_in: 1 month
        when: always
    
    after_script:
      - ls -la *
      - more *.log
    ```
